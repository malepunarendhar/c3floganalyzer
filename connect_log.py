import result_data_logs
import pandas as pd
import matplotlib.pyplot as plt


def tag_connected_log(node, time_stamp):
    node_data = result_data_logs.tags_data[node]
    connected_data = node_data["connect"]
    connected_time_diff = time_stamp - connected_data["timestamp"]
    if "cnntedTimeDiffCnt" in connected_data.keys():
        time_diff_cnt = connected_data["cnntedTimeDiffCnt"]
        if connected_time_diff in time_diff_cnt.keys():
            cnt = time_diff_cnt[connected_time_diff] + 1
            time_diff_cnt.update([(connected_time_diff, cnt)])
        else:
            time_diff_cnt[connected_time_diff] = 1
        connected_data.update([("cnntedTimeDiffCnt", time_diff_cnt)])
    else:
        time_diff_cnt = dict([(connected_time_diff, 1)])
        connected_data["cnntedTimeDiffCnt"] = time_diff_cnt
    connected_data_cnt = connected_data["connectedCnt"] + 1
    connected_data.update([("connectedCnt", connected_data_cnt)])
    connected_data.update([("cnntedTimestamp", time_stamp)])
    node_data.update([("connect", connected_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def connect_percentage_log(node_data):
    if "connect" in node_data.keys():
        node_connect_data = node_data["connect"]
        cnt = node_connect_data["connectCnt"]
        if "timestamp" in node_connect_data.keys():
            node_connect_data.pop("timestamp")
        connected_cnt = node_connect_data["connectedCnt"]
        connected_avg_cnt = (connected_cnt / cnt) * 100
        connected_avg_cnt = round(connected_avg_cnt, 2)
        node_connect_data.update([("connectedCnt", [connected_cnt, connected_avg_cnt])])
        if "cnntedTimeDiffCnt" in node_connect_data.keys():
            connected_time_diff = node_connect_data["cnntedTimeDiffCnt"]
            for connected_key in connected_time_diff.keys():
                time_diff_cnt = connected_time_diff[connected_key]
                avg_cnt = (time_diff_cnt / cnt) * 100
                avg_cnt = round(avg_cnt, 2)
                connected_time_diff.update([(connected_key, [time_diff_cnt, avg_cnt])])


def connected_log(node, time_stamp):
    node_data = result_data_logs.tags_data[node]
    node_connect_data = node_data["connect"]
    if "connectedCnt" in node_connect_data.keys():
        tag_connected_log(node, time_stamp)
    else:
        node_connect_data["connectedCnt"] = 1
        node_connect_data["cnntedTimestamp"] = time_stamp
        node_data.update([("connect", node_connect_data)])
        result_data_logs.tags_data[node] = node_data


def connect_log(node, time_stamp):
    node_data = result_data_logs.tags_data[node]
    if "connect" in node_data.keys():
        connect_data = node_data["connect"]
        connect_data_cnt = connect_data["connectCnt"] + 1
        connect_data.update([("connectCnt", connect_data_cnt)])
        connect_data.update([("timestamp", time_stamp)])
        node_data.update([("connect", connect_data)])
        result_data_logs.tags_data.update([(node, node_data)])
    else:
        connect_data = dict([("connectCnt", 1), ("timestamp", time_stamp)])
        node_data.update([("connect", connect_data)])
        result_data_logs.tags_data[node] = node_data
