import logging
import tags_log
import jsons_log
import result_data_logs


def main():
    logging.basicConfig(level=logging.INFO,
                        format='%(created)f %(levelname)s [%(filename)s->%(funcName)s:%(lineno)d] %(message)s')
    log_file = open("logs/gw_log.log", 'r')
    ac_log_lines = log_file.readlines()
    log_file.close()
    end_time = 0
    start_time = 0
    size = len(ac_log_lines) - 1
    count = 0
    while count < size:
        line = ac_log_lines[count].split()
        line = ' '.join(line).split()
        if len(line) > 0:
            first_line = line[0]
            if first_line.isdigit():
                time_stamp = int(first_line)
                if start_time == 0:
                    start_time = time_stamp
                elif (time_stamp - start_time) > 1800:
                    del ac_log_lines[count : (size + 1)]
                    break
        count += 1
    count = len(ac_log_lines) - 1
    while count > 0:
        line = ac_log_lines[count].split()
        line = ' '.join(line).split()
        if len(line) > 0:
            last_line = line[0]
            if last_line.isdigit():
                end_time = int(last_line)
                break
        count -= 1
    time_recorded = end_time - start_time
    logging.info("Total log recorded time " + str(time_recorded) + " seconds")
    result_data_logs.logs_data["recorded_time"] = time_recorded
    tags_log.tags_log(ac_log_lines)
    json_log_file = open("logs/mqtt_log.log", 'r')
    json_log_lines = json_log_file.readlines()
    json_log_file.close()
    jsons_log.json_parse_log(json_log_lines)
    

if __name__ == "__main__":
    main()
