import logging
import json
import result_data_logs
import adv_log
import connect_log
import disconnect_log
import notification_log
import read_log
import cloud_log
import os
import shutil


def cloud_counts(tags_cnt, time_stamp):
    if "cloud_counts" in result_data_logs.logs_data.keys():
        cloud_cnts = result_data_logs.logs_data["cloud_counts"]
        cld_cnt = cloud_cnts["cnt"]
        if time_stamp == cloud_cnts["timestamp"]:
            cld_cnt = cld_cnt + tags_cnt
        else:
            if "DiffCnt" in cloud_cnts.keys():
                diff_cnt = cloud_cnts["DiffCnt"]
                if cld_cnt in diff_cnt.keys():
                    cnt = diff_cnt[cld_cnt]
                    cnt += tags_cnt
                    diff_cnt.update([(cld_cnt, cnt)])
                else:
                    diff_cnt[cld_cnt] = tags_cnt
                cloud_cnts.update([("DiffCnt", diff_cnt)])
            else:
                cld_diff_cnt = dict([(cld_cnt, tags_cnt)])
                cloud_cnts["DiffCnt"] = cld_diff_cnt
            cld_cnt = tags_cnt
        cloud_cnts.update([("cnt", cld_cnt)])
        cloud_cnts.update([("timestamp", time_stamp)])
    else:
        cld_data = dict([("cnt", 1), ("timestamp", time_stamp)])
        result_data_logs.logs_data.update([("cloud_counts", cld_data)])


def adv_counts(adv_count, time_stamp):
    if adv_count in result_data_logs.logs_data.keys():
        adv_cnts = result_data_logs.logs_data[adv_count]
        act_cnt = adv_cnts["cnt"]
        if time_stamp == adv_cnts["timestamp"]:
            adv_cnt = act_cnt + 1
        else:
            if "DiffCnt" in adv_cnts.keys():
                diff_cnt = adv_cnts["DiffCnt"]
                if act_cnt in diff_cnt.keys():
                    cnt = diff_cnt[act_cnt]
                    cnt += 1
                    diff_cnt.update([(act_cnt, cnt)])
                else:
                    diff_cnt[act_cnt] = 1
                adv_cnts.update([("DiffCnt", diff_cnt)])
            else:
                act_diff_cnt = dict([(act_cnt, 1)])
                adv_cnts["DiffCnt"] = act_diff_cnt
            adv_cnt = 1
        adv_cnts.update([("cnt", adv_cnt)])
        adv_cnts.update([("timestamp", time_stamp)])
    else:
        adv_data = dict([("cnt", 1), ("timestamp", time_stamp)])
        result_data_logs.logs_data.update([(adv_count, adv_data)])
        

def tags_log(log_lines):
    size = len(log_lines)
    count = 0
    while count < size:
        line = log_lines[count].split(' ')
        line = ' '.join(line).split()
        if len(line) > 5 and line[3] == "BLE":
            time_stamp = int(line[0])
            if line[6] == "RSSI":
                if line[1] == "INFO":
                    adv_counts("adv_counts", time_stamp)
                elif line[1] == "INFBLE":
                    adv_counts("adv_read_counts", time_stamp)
            elif line[4] == "Tags":
                cld_cnt = line[5]
                cld_cnt = cld_cnt.split('[')[1].split(']')[0]
                cld_cnt_int = int(cld_cnt)
                cloud_counts(cld_cnt_int, time_stamp)
            if line[4] != "Node":
                count += 1
                continue
            node = line[5]
            node = node.split('[')[1].split(']')[0]
            if line[6] == "RSSI":
                if line[1] == "INFO":
                    adv_log.adv_log(node, time_stamp)
                elif line[1] == "INFBLE":
                    adv_log.adv_read_log(node, time_stamp)
            elif line[6] == "ConnectReq":
                connect_log.connect_log(node, time_stamp)
            elif line[6] == "Connected":
                connect_log.connected_log(node, time_stamp)
            elif line[6] == "DisconnectReq":
                disconnect_log.disconnect_log(node, time_stamp)
            elif line[6] == "Disconnected":
                disconnect_log.disconnected_log(node, time_stamp)
            elif line[6] == "ReadReq":
                sensor_id = int(line[8])
                read_log.read_request_log(node, time_stamp, sensor_id)
            elif line[6] == "ReadResp":
                sensor_id = int(line[8])
                read_log.read_response_log(node, time_stamp, sensor_id)
            elif line[6] == "Notification" and line[7] == "Enabled":
                sensor_id = int(line[9])
                notification_log.notification_enable_log(node, time_stamp, sensor_id)
            elif line[6] == "Notification" and line[7] == "Disabled":
                sensor_id = int(line[9])
                notification_log.notification_disable_log(node, time_stamp, sensor_id)
            elif line[6] == "Cloud":
                micro_seconds = line[8]
                micro_seconds = micro_seconds.split('[')[1].split(']')[0]
                cloud_log.cloud_count(node, micro_seconds, time_stamp)
        count += 1
    for key in result_data_logs.tags_data.keys():
        node_data = result_data_logs.tags_data[key]
        adv_log.adv_percentage_log(node_data)
        connect_log.connect_percentage_log(node_data)
        disconnect_log.disconnect_percentage_log(node_data)
        read_log.read_percentage_log(node_data)
        notification_log.notification_percentage_log(node_data)
        try:    
            path = "logs/results/" + key
            if os.path.exists(path):
                shutil.rmtree(path)
            os.makedirs(path)
            with open(path + '/log_results.json', 'w') as json_file:
                json.dump(node_data, json_file)
            adv_log.adv_plot_log(path, key)
        except OSError as error:
            print(error)
    try:
        path = "logs/results/" + "1c3f_results"
        if os.path.exists(path):
            shutil.rmtree(path)
        os.makedirs(path)
        with open(path + '/log_results.json', 'w') as json_file:
            json.dump(result_data_logs.logs_data, json_file)
    except OSError as error:
        print(error)
    logging.info("Copied log result as json")
