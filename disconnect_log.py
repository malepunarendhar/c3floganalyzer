import result_data_logs


def tag_disconnected_log(node, time_stamp):
    node_data = result_data_logs.tags_data[node]
    disconnected_data = node_data["disconnect"]
    disconnected_time_diff = time_stamp - disconnected_data["timestamp"]
    if "dcnntedTimeDiffCnt" in disconnected_data.keys():
        time_diff_cnt = disconnected_data["dcnntedTimeDiffCnt"]
        if disconnected_time_diff in time_diff_cnt.keys():
            cnt = time_diff_cnt[disconnected_time_diff] + 1
            time_diff_cnt.update([(disconnected_time_diff, cnt)])
        else:
            time_diff_cnt[disconnected_time_diff] = 1
        disconnected_data.update([("dcnntedTimeDiffCnt", time_diff_cnt)])
    else:
        time_diff_cnt = dict([(disconnected_time_diff, 1)])
        disconnected_data["dcnntedTimeDiffCnt"] = time_diff_cnt
    disconnected_data_cnt = disconnected_data["dconnectedCnt"] + 1
    disconnected_data.update([("dconnectedCnt", disconnected_data_cnt)])
    node_data.update([("disconnect", disconnected_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def disconnect_percentage_log(node_data):
    if "disconnect" in node_data.keys():
        node_disconnect_data = node_data["disconnect"]
        cnt = node_disconnect_data["dconnectCnt"]
        if "timestamp" in node_disconnect_data.keys():
            node_disconnect_data.pop("timestamp")
        if "dcnntedTimeDiffCnt" in node_disconnect_data.keys():
            disconnected_time_diff = node_disconnect_data["dcnntedTimeDiffCnt"]
            for diff_key in disconnected_time_diff.keys():
                time_diff_cnt = disconnected_time_diff[diff_key]
                avg_cnt = (time_diff_cnt / cnt) * 100
                avg_cnt = round(avg_cnt, 2)
                disconnected_time_diff.update([(diff_key, [time_diff_cnt, avg_cnt])])


def disconnected_log(node, time_stamp):
    node_data = result_data_logs.tags_data[node]
    node_disconnect_data = node_data["disconnect"]
    if "dconnectedCnt" in node_disconnect_data.keys():
        tag_disconnected_log(node, time_stamp)
    else:
        node_disconnect_data["dconnectedCnt"] = 1
        node_data.update([("disconnect", node_disconnect_data)])
        result_data_logs.tags_data[node] = node_data


def disconnect_log(node, time_stamp):
    node_data = result_data_logs.tags_data[node]
    if "disconnect" in node_data.keys():
        disconnect_data = node_data["disconnect"]
        disconnect_data_cnt = disconnect_data["dconnectCnt"] + 1
        disconnect_data.update([("dconnectCnt", disconnect_data_cnt)])
        disconnect_data.update([("timestamp", time_stamp)])
        node_data.update([("disconnect", disconnect_data)])
        result_data_logs.tags_data.update([(node, node_data)])
    else:
        disconnect_data = dict([("dconnectCnt", 1), ("timestamp", time_stamp)])
        node_data.update([("disconnect", disconnect_data)])
        result_data_logs.tags_data[node] = node_data
