import result_data_logs
import pandas as pd
import matplotlib.pyplot as plt


def tag_adv_read_log(node, time_stamp):
    node_data = result_data_logs.tags_data[node]
    adv_data = node_data["adv_read"]
    time_diff = time_stamp - adv_data["timestamp"]
    if "timeDiffCnt" in adv_data.keys():
        time_diff_cnt = adv_data["timeDiffCnt"]
        if time_diff in time_diff_cnt.keys():
            cnt = time_diff_cnt[time_diff]
            cnt += 1
            time_diff_cnt.update([(time_diff, cnt)])
        else:
            time_diff_cnt[time_diff] = 1
        adv_data.update([("timeDiffCnt", time_diff_cnt)])
    else:
        time_diff_cnt = dict([(time_diff, 1)])
        adv_data["timeDiffCnt"] = time_diff_cnt
    adv_data_cnt = adv_data["cnt"] + 1
    adv_data.update([("cnt", adv_data_cnt)])
    adv_data.update([("timestamp", time_stamp)])
    node_data.update([("adv_read", adv_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def tag_adv_log(node, time_stamp):
    node_data = result_data_logs.tags_data[node]
    adv_data = node_data["adv"]
    time_diff = time_stamp - adv_data["timestamp"]
    if "timeDiffCnt" in adv_data.keys():
        time_diff_cnt = adv_data["timeDiffCnt"]
        if time_diff in time_diff_cnt.keys():
            cnt = time_diff_cnt[time_diff]
            cnt += 1
            time_diff_cnt.update([(time_diff, cnt)])
        else:
            time_diff_cnt[time_diff] = 1
        adv_data.update([("timeDiffCnt", time_diff_cnt)])
    else:
        time_diff_cnt = dict([(time_diff, 1)])
        adv_data["timeDiffCnt"] = time_diff_cnt
    adv_data_cnt = adv_data["cnt"] + 1
    adv_data.update([("cnt", adv_data_cnt)])
    adv_data.update([("timestamp", time_stamp)])
    node_data.update([("adv", adv_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def adv_percentage_log(node_data):
    if "adv" in node_data.keys():
        node_adv_data = node_data["adv"]
        cnt = node_adv_data["cnt"]
        if "timestamp" in node_adv_data.keys():
            node_adv_data.pop("timestamp")
        if "timeDiffCnt" in node_adv_data.keys():
            node_time_diff = node_adv_data["timeDiffCnt"]
            for diff_key in node_time_diff.keys():
                time_diff_cnt = node_time_diff[diff_key]
                avg_cnt = (time_diff_cnt / cnt) * 100
                avg_cnt = round(avg_cnt, 2)
                node_time_diff.update([(diff_key, [time_diff_cnt, avg_cnt])])
    if "adv_read" in node_data.keys():
        node_adv_data = node_data["adv_read"]
        cnt = node_adv_data["cnt"]
        if "timestamp" in node_adv_data.keys():
            node_adv_data.pop("timestamp")
        if "timeDiffCnt" in node_adv_data.keys():
            node_time_diff = node_adv_data["timeDiffCnt"]
            for diff_key in node_time_diff.keys():
                time_diff_cnt = node_time_diff[diff_key]
                avg_cnt = (time_diff_cnt / cnt) * 100
                avg_cnt = round(avg_cnt, 2)
                node_time_diff.update([(diff_key, [time_diff_cnt, avg_cnt])])


def adv_log(node, time_stamp):
    if node in result_data_logs.tags_data.keys():
        a_data = result_data_logs.tags_data[node]
        if "adv" in a_data.keys():
            tag_adv_log(node, time_stamp)
        else:
            adv_data = dict([("cnt", 1), ("timestamp", time_stamp)])
            a_data["adv"] = adv_data
            result_data_logs.tags_data.update([(node, a_data)])
    else:
        adv_data = dict([("cnt", 1), ("timestamp", time_stamp)])
        node_data = dict([("adv", adv_data)])
        result_data_logs.tags_data[node] = node_data


def adv_read_log(node, time_stamp):
    if node in result_data_logs.tags_data.keys():
        a_data = result_data_logs.tags_data[node]
        if "adv_read" in a_data.keys():
            tag_adv_read_log(node, time_stamp)
        else:
            adv_data = dict([("cnt", 1), ("timestamp", time_stamp)])
            node_data = dict([("adv_read", adv_data)])
            result_data_logs.tags_data.update([(node, node_data)])
    else:
        adv_read_data = dict([("cnt", 1), ("timestamp", time_stamp)])
        node_data = dict([("adv_read", adv_read_data)])
        result_data_logs.tags_data[node] = node_data


def adv_plot_log(path, node):
    node_data = result_data_logs.tags_data[node]
    node_adv_data = node_data["adv"]
    adv_diff_cnt = node_adv_data["timeDiffCnt"]
    adv_data = []
    for key in adv_diff_cnt.keys():
        data = [key, adv_diff_cnt[key][0]]
        adv_data.append(data)
    df = pd.DataFrame(adv_data, columns=['TimeDiff', 'Count'])
    plt.title(node)
    plt.bar(df['TimeDiff'], df['Count'])
    plt.xlabel('TimeDiff')
    plt.ylabel('Count')
    plt.savefig(path + "/adv_data_count.png")
    plt.close()
    adv_data = []
    for key in adv_diff_cnt.keys():
        data = [key, adv_diff_cnt[key][1]]
        adv_data.append(data)
    df = pd.DataFrame(adv_data, columns=['TimeDiff', 'Percentage'])
    plt.title(node)
    plt.bar(df['TimeDiff'], df['Percentage'])
    plt.xlabel('TimeDiff')
    plt.ylabel('Percentage')
    plt.savefig(path + "/adv_data_percentage.png")
    plt.close()


def adv_read_plot_log(path, node):
    node_data = result_data_logs.tags_data[node]
    node_adv_data = node_data["adv_read"]
    adv_diff_cnt = node_adv_data["timeDiffCnt"]
    adv_data = []
    for key in adv_diff_cnt.keys():
        data = [key, adv_diff_cnt[key][0]]
        adv_data.append(data)
    df = pd.DataFrame(adv_data, columns=['TimeDiff', 'Count'])
    plt.title(node)
    plt.bar(df['TimeDiff'], df['Count'])
    plt.xlabel('TimeDiff')
    plt.ylabel('Count')
    plt.savefig(path + "/adv_read_data_count.png")
    plt.close()
    adv_data = []
    for key in adv_diff_cnt.keys():
        data = [key, adv_diff_cnt[key][1]]
        adv_data.append(data)
    df = pd.DataFrame(adv_data, columns=['TimeDiff', 'Percentage'])
    plt.title(node)
    plt.bar(df['TimeDiff'], df['Percentage'])
    plt.xlabel('TimeDiff')
    plt.ylabel('Percentage')
    plt.savefig(path + "/adv_read_data_percentage.png")
    plt.close()
