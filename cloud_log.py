import result_data_logs


def tag_proc_count(node, micro_seconds):
    node_data = result_data_logs.tags_data[node]
    proc_data = node_data["proc_data"]
    diff_cnt = proc_data["diffCnt"]
    if micro_seconds in diff_cnt.keys():
        cnt = diff_cnt[micro_seconds]
        cnt += 1
        diff_cnt.update([(micro_seconds, cnt)])
    else:
        diff_cnt[micro_seconds] = 1
    proc_data.update([("diffCnt", diff_cnt)])
    node_data.update([("proc_data", proc_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def tag_cloud_report_count(node, micro_seconds, time_stamp):
    node_data = result_data_logs.tags_data[node]
    cloud_data = node_data["cloud_data"]
    time_diff = time_stamp - cloud_data["timestamp"]
    if "timeDiffCnt" in cloud_data.keys():
        time_diff_cnt = cloud_data["timeDiffCnt"]
        if time_diff in time_diff_cnt.keys():
            cnt = time_diff_cnt[time_diff]
            cnt += 1
            time_diff_cnt.update([(time_diff, cnt)])
        else:
            time_diff_cnt[time_diff] = 1
        cloud_data.update([("timeDiffCnt", time_diff_cnt)])
    else:
        time_diff_cnt = dict([(time_diff, 1)])
        cloud_data["timeDiffCnt"] = time_diff_cnt
    cloud_data_cnt = cloud_data["cnt"] + 1
    cloud_data.update([("cnt", cloud_data_cnt)])
    cloud_data.update([("timestamp", time_stamp)])
    node_data.update([("cloud_data", cloud_data)])
    result_data_logs.tags_data.update([(node, node_data)])
    tag_proc_count(node, micro_seconds)


def cloud_count(node, micro_seconds, time_stamp):
    if node in result_data_logs.tags_data.keys():
        node_data = result_data_logs.tags_data[node]
        if "cloud_data" in node_data.keys():
            tag_cloud_report_count(node, micro_seconds, time_stamp)
        else:
            node_cloud_data = dict([("cnt", 1), ("timestamp", time_stamp)])
            node_data["cloud_data"] = node_cloud_data
            node_proc_data = dict([(micro_seconds, 1)])
            node_data["proc_data"] = dict([("diffCnt", node_proc_data)])
            result_data_logs.tags_data[node] = node_data