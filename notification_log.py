import result_data_logs


def tag_notification_enabled_log(node, time_stamp, sensor_id):
    node_data = result_data_logs.tags_data[node]
    notification_data = node_data["notification"]
    if "notificationEn" in notification_data.keys():
        notification_en_data = notification_data["notificationEn"]
        if sensor_id in notification_en_data.keys():
            notification_en_sensor_data = notification_en_data[sensor_id]
            notification_time_diff = time_stamp - notification_en_sensor_data["timestamp"]
            notification_data_cnt = notification_en_sensor_data["notificationCnt"] + 1
            notification_en_sensor_data.update([("notificationCnt", notification_data_cnt)])
            if "notificationTimeDiffCnt" in notification_en_sensor_data.keys():
                time_diff_cnt = notification_en_sensor_data["notificationTimeDiffCnt"]
                if notification_time_diff in time_diff_cnt.keys():
                    cnt = time_diff_cnt[notification_time_diff] + 1
                    time_diff_cnt.update([(notification_time_diff, cnt)])
                else:
                    time_diff_cnt[notification_time_diff] = 1
                notification_en_sensor_data.update([("notificationTimeDiffCnt", time_diff_cnt)])
                notification_en_sensor_data.update([("timestamp", time_stamp)])
                notification_en_data[sensor_id] = notification_en_sensor_data
            else:
                notification_en_sensor_data["notificationTimeDiffCnt"] = dict([(notification_time_diff, 1)])
        else:
            notification_en_sensor_data = dict([("notificationCnt", 1), ("timestamp", time_stamp)])
            notification_en_data[sensor_id] = notification_en_sensor_data
            notification_data.update([("notificationEn", notification_en_data)])
    else:
        notification_en_sensor_data = dict([("notificationCnt", 1), ("timestamp", time_stamp)])
        notification_en_data = dict([(sensor_id, notification_en_sensor_data)])
        notification_data["notificationEn"] = notification_en_data
    notification_data_cnt = notification_data["notificationCnt"] + 1
    notification_data.update([("notificationCnt", notification_data_cnt)])
    node_data.update([("notification", notification_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def tag_notification_disabled_log(node, time_stamp, sensor_id):
    node_data = result_data_logs.tags_data[node]
    notification_data = node_data["notification"]
    if "notificationDs" in notification_data.keys():
        notification_ds_data = notification_data["notificationDs"]
        notification_en_data = notification_data["notificationEn"]
        if sensor_id in notification_ds_data.keys():
            notification_ds_sensor_data = notification_ds_data[sensor_id]
            notification_en_sensor_data = notification_en_data[sensor_id]
            notification_time_diff = time_stamp - notification_en_sensor_data["timestamp"]
            notification_data_cnt = notification_ds_sensor_data["notificationCnt"] + 1
            notification_ds_sensor_data.update([("notificationCnt", notification_data_cnt)])
            if "notificationTimeDiffCnt" in notification_ds_sensor_data.keys():
                time_diff_cnt = notification_ds_sensor_data["notificationTimeDiffCnt"]
                if notification_time_diff in time_diff_cnt.keys():
                    cnt = time_diff_cnt[notification_time_diff] + 1
                    time_diff_cnt.update([(notification_time_diff, cnt)])
                else:
                    time_diff_cnt[notification_time_diff] = 1
                notification_ds_sensor_data.update([("notificationTimeDiffCnt", time_diff_cnt)])
                notification_ds_data[sensor_id] = notification_ds_sensor_data
            else:
                notification_ds_sensor_data["notificationTimeDiffCnt"] = dict([(notification_time_diff, 1)])
        else:
            notification_ds_sensor_data = dict([("notificationCnt", 1)])
            notification_ds_data[sensor_id] = notification_ds_sensor_data
    else:
        notification_ds_sensor_data = dict([("notificationCnt", 1)])
        notification_ds_data = dict([(sensor_id, notification_ds_sensor_data)])
        notification_data["notificationDs"] = notification_ds_data
    notification_data_cnt = notification_data["notificationCnt"] + 1
    notification_data.update([("notificationCnt", notification_data_cnt)])
    node_data.update([("notification", notification_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def notification_enable_log(node, time_stamp, sensor_id):
    if node in result_data_logs.tags_data.keys():
        node_data = result_data_logs.tags_data[node]
        if "notification" in node_data.keys():
            tag_notification_enabled_log(node, time_stamp, sensor_id)
        else:
            node_notification_data = dict([("notificationCnt", 1)])
            node_data["notification"] = node_notification_data
            result_data_logs.tags_data[node] = node_data


def notification_disable_log(node, time_stamp, sensor_id):
    if node in result_data_logs.tags_data.keys():
        tag_notification_disabled_log(node, time_stamp, sensor_id)


def notification_percentage_log(node_data):
    if "notification" in node_data.keys():
        node_notification_data = node_data["notification"]
        if "notificationEn" in node_notification_data.keys():
            notification_enable_data = node_notification_data["notificationEn"]
            for key in notification_enable_data.keys():
                sensor_data = notification_enable_data[key]
                sensor_data.pop("timestamp")
                cnt = sensor_data["notificationCnt"]
                node_time_diff = sensor_data["notificationTimeDiffCnt"]
                for diff_key in node_time_diff.keys():
                    time_diff_cnt = node_time_diff[diff_key]
                    avg_cnt = (time_diff_cnt / cnt) * 100
                    avg_cnt = round(avg_cnt, 2)
                    node_time_diff.update([(diff_key, [time_diff_cnt, avg_cnt])])
        if "notificationDs" in node_notification_data.keys():
            notification_disable_data = node_notification_data["notificationDs"]
            for key in notification_disable_data.keys():
                sensor_data = notification_disable_data[key]
                cnt = sensor_data["notificationCnt"]
                node_time_diff = sensor_data["notificationTimeDiffCnt"]
                for diff_key in node_time_diff.keys():
                    time_diff_cnt = node_time_diff[diff_key]
                    avg_cnt = (time_diff_cnt / cnt) * 100
                    avg_cnt = round(avg_cnt, 2)
                    node_time_diff.update([(diff_key, [time_diff_cnt, avg_cnt])])
