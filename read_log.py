import logging
import result_data_logs


def tag_read_request_log(node, time_stamp, sensor_id):
    node_data = result_data_logs.tags_data[node]
    read_data = node_data["read"]
    if "readReq" in read_data.keys():
        read_req_data = read_data["readReq"]
        if sensor_id in read_req_data.keys():
            read_req_sensor_data = read_req_data[sensor_id]
            read_time_diff = time_stamp - read_req_sensor_data["timestamp"]
            read_data_cnt = read_req_sensor_data["readCnt"] + 1
            read_req_sensor_data.update([("readCnt", read_data_cnt)])
            if "readTimeDiffCnt" in read_req_sensor_data.keys():
                time_diff_cnt = read_req_sensor_data["readTimeDiffCnt"]
                if read_time_diff in time_diff_cnt.keys():
                    cnt = time_diff_cnt[read_time_diff] + 1
                    time_diff_cnt.update([(read_time_diff, cnt)])
                else:
                    time_diff_cnt[read_time_diff] = 1
                read_req_sensor_data.update([("readTimeDiffCnt", time_diff_cnt)])
                read_req_sensor_data.update([("timestamp", time_stamp)])
                read_req_data[sensor_id] = read_req_sensor_data
            else:
                read_req_sensor_data["readTimeDiffCnt"] = dict([(read_time_diff, 1)])
        else:
            read_req_sensor_data = dict([("readCnt", 1), ("timestamp", time_stamp)])
            read_req_data[sensor_id] = read_req_sensor_data
            read_data.update([("readReq", read_req_data)])
    else:
        read_req_sensor_data = dict([("readCnt", 1), ("timestamp", time_stamp)])
        read_req_data = dict([(sensor_id, read_req_sensor_data)])
        read_data["readReq"] = read_req_data
    read_data_cnt = read_data["readCnt"] + 1
    read_data.update([("readCnt", read_data_cnt)])
    node_data.update([("read", read_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def tag_read_response_log(node, time_stamp, sensor_id):
    node_data = result_data_logs.tags_data[node]
    read_data = node_data["read"]
    if "readResp" in read_data.keys():
        read_resp_data = read_data["readResp"]
        read_req_data = read_data["readReq"]
        if sensor_id in read_resp_data.keys():
            read_resp_sensor_data = read_resp_data[sensor_id]
            read_req_sensor_data = read_req_data[sensor_id]
            read_time_diff = time_stamp - read_req_sensor_data["timestamp"]
            read_data_cnt = read_resp_sensor_data["readCnt"] + 1
            read_resp_sensor_data.update([("readCnt", read_data_cnt)])
            if "readTimeDiffCnt" in read_resp_sensor_data.keys():
                time_diff_cnt = read_resp_sensor_data["readTimeDiffCnt"]
                if read_time_diff in time_diff_cnt.keys():
                    cnt = time_diff_cnt[read_time_diff] + 1
                    time_diff_cnt.update([(read_time_diff, cnt)])
                else:
                    time_diff_cnt[read_time_diff] = 1
                read_resp_sensor_data.update([("readTimeDiffCnt", time_diff_cnt)])
                read_resp_data[sensor_id] = read_resp_sensor_data
            else:
                read_resp_sensor_data["readTimeDiffCnt"] = dict([(read_time_diff, 1)])
        else:
            read_resp_sensor_data = dict([("readCnt", 1)])
            read_resp_data[sensor_id] = read_resp_sensor_data
    else:
        read_resp_sensor_data = dict([("readCnt", 1)])
        read_resp_data = dict([(sensor_id, read_resp_sensor_data)])
        read_data["readResp"] = read_resp_data
    read_data_cnt = read_data["readCnt"] + 1
    read_data.update([("readCnt", read_data_cnt)])
    node_data.update([("read", read_data)])
    result_data_logs.tags_data.update([(node, node_data)])


def read_request_log(node, time_stamp, sensor_id):
    if node in result_data_logs.tags_data.keys():
        node_data = result_data_logs.tags_data[node]
        if "read" in node_data.keys():
            tag_read_request_log(node, time_stamp, sensor_id)
        else:
            node_read_data = dict([("readCnt", 1)])
            node_data["read"] = node_read_data
            result_data_logs.tags_data[node] = node_data


def read_response_log(node, time_stamp, sensor_id):
    if node in result_data_logs.tags_data.keys():
        tag_read_response_log(node, time_stamp, sensor_id)


def read_percentage_log(node_data):
    if "read" in node_data.keys():
        node_read_data = node_data["read"]
        if "readReq" in node_read_data.keys():
            node_read_req_data = node_read_data["readReq"]
            for key in node_read_req_data.keys():
                sensor_data = node_read_req_data[key]
                sensor_data.pop("timestamp")
                cnt = sensor_data["readCnt"]
                node_time_diff = sensor_data["readTimeDiffCnt"]
                for diff_key in node_time_diff.keys():
                    time_diff_cnt = node_time_diff[diff_key]
                    avg_cnt = (time_diff_cnt / cnt) * 100
                    avg_cnt = round(avg_cnt, 2)
                    node_time_diff.update([(diff_key, [time_diff_cnt, avg_cnt])])
        if "readResp" in node_read_data.keys():
            node_read_resp_data = node_read_data["readResp"]
            for key in node_read_resp_data.keys():
                sensor_data = node_read_resp_data[key]
                cnt = sensor_data["readCnt"]
                if "readTimeDiffCnt" in sensor_data.keys():
                    node_time_diff = sensor_data["readTimeDiffCnt"]
                    for diff_key in node_time_diff.keys():
                        time_diff_cnt = node_time_diff[diff_key]
                        avg_cnt = (time_diff_cnt / cnt) * 100
                        avg_cnt = round(avg_cnt, 2)
                        node_time_diff.update([(diff_key, [time_diff_cnt, avg_cnt])])
