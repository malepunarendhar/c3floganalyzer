import logging
import json
import result_data_logs


def device_percentage_log(node_data):
    if "attr" in node_data.keys():
        node_attr = node_data["attr"]
        for key in node_attr.keys():
            sensor_data = node_attr[key]
            cnt = 1
            if "timestamp" in sensor_data.keys():
                sensor_data.pop("timestamp")
            if "cnt" in sensor_data.keys():
                cnt = sensor_data["cnt"]
            if "timeDiffCnt" in sensor_data.keys():
                node_time_diff = sensor_data["timeDiffCnt"]
                for diff_key in node_time_diff.keys():
                    time_diff_cnt = node_time_diff[diff_key]
                    avg_cnt = (time_diff_cnt / cnt) * 100
                    avg_cnt = round(avg_cnt, 2)
                    node_time_diff.update([(diff_key, [time_diff_cnt, avg_cnt])])


def device_data_parse(node, device_attr):
    node_data = result_data_logs.json_dict_data[node]
    attr_name = device_attr["name"]
    attr_timestamp = device_attr["sTS"]
    if "attr" in node_data.keys():
        node_attr = node_data["attr"]
        if attr_name in node_attr.keys():
            node_attr_data = node_attr[attr_name]
            time_diff = attr_timestamp - node_attr_data["timestamp"]
            node_attr_data.update([("timestamp", attr_timestamp)])
            cnt = node_attr_data["cnt"] + 1
            node_attr_data.update([("cnt", cnt)])
            if "timeDiffCnt" in node_attr_data.keys():
                time_diff_cnt = node_attr_data["timeDiffCnt"]
                if time_diff in time_diff_cnt.keys():
                    cnt = time_diff_cnt[time_diff] + 1
                    time_diff_cnt.update([(time_diff, cnt)])
                else:
                    time_diff_cnt[time_diff] = 1
                node_attr_data.update([("timeDiffCnt", time_diff_cnt)])
            else:
                time_diff_cnt = dict([(time_diff, 1)])
                node_attr_data["timeDiffCnt"] = time_diff_cnt
        else:
            node_attr_data = dict([("cnt", 1), ("timestamp", attr_timestamp)])
            node_attr[attr_name] = node_attr_data
    else:
        node_attr_data = dict([("cnt", 1), ("timestamp", attr_timestamp)])
        node_attr = dict([(attr_name, node_attr_data)])
        node_data["attr"] = node_attr
    result_data_logs.json_dict_data.update([(node, node_data)])


def json_parse_log(json_log_lines):
    size = len(json_log_lines)
    count = 0
    while count < size:
        line = json_log_lines[count]
        try:
            json_line = json.loads(line)
            device_msg_data = json_line["msg"]
            devices_cnt = len(device_msg_data) - 1
            while devices_cnt >= 0:
                device_data = json_line["msg"][devices_cnt]
                node = device_data["dPID"]
                if node not in result_data_logs.json_dict_data.keys():
                    device_logical_id = dict([("dLID", device_data["dLID"])])
                    result_data_logs.json_dict_data[node] = device_logical_id
                device_attr_cnt = len(device_data["attr"]) - 1
                while device_attr_cnt >= 0:
                    device_attr = device_data["attr"][device_attr_cnt]
                    device_data_parse(node, device_attr)
                    device_attr_cnt -= 1
                devices_cnt -= 1
        except:
            logging.info("Invalid Json")
        count += 1
    for key in result_data_logs.json_dict_data.keys():
        node_data = result_data_logs.json_dict_data[key]
        device_percentage_log(node_data)
    node_data = result_data_logs.json_dict_data["80EACA800016"]
    with open('logs/json_result.json', 'w') as json_file:
        json.dump(node_data, json_file)
    logging.info("Copied json result to file as json")
